package com.example.demo

import groovy.transform.CompileStatic
import org.codehaus.groovy.runtime.ScriptBytecodeAdapter
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@CompileStatic
@RestController
public class TestController implements GroovyObject {
    @GetMapping("/test")
    public String test() {
        return "success"
    }
}